/***************************************************************************
**  Initial code taken from: http://www.labsquare.org (Sacha Schutz)      **
** Where can be drawn a grid and that object could paint the desired cell **
****************************************************************************
**                                                                        **
**  GridView, a simple GridView made with Qt4                             **
**  Copyright (C) 2013 Sacha Schutz                                       **
**                                                                        **
**  This program is free software: you can redistribute it and/or modify  **
**  it under the terms of the GNU General Public License as published by  **
**  the Free Software Foundation, either version 3 of the License, or     **
**  (at your option) any later version.                                   **
**                                                                        **
**  This program is distributed in the hope that it will be useful,       **
**  but WITHOUT ANY WARRANTY; without even the implied warranty of        **
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         **
**  GNU General Public License for more details.                          **
**                                                                        **
**  You should have received a copy of the GNU General Public License     **
**  along with this program.  If not, see http://www.gnu.org/licenses/.   **
**                                                                        **
****************************************************************************/

//The rest of the code was implemented by Ramon Collazo for the project 
//TUES at UPRRP.

#include "grid.h"
#include <QVBoxLayout>
#include <QToolBar>
#include <QtGlobal>
#include <QDebug>
#include <QtCore>

GridWidget::GridWidget(QWidget *parent):
    QWidget(parent)
{
    mCellSize = 10;
    mRowCount = 39;
    mColumnCount = 27;

    Tool = "dot";
    ToolLength = 1;
    frontColor = Qt::black;
    backColor = QColor::Invalid;
    background = Qt::white;
    emit canUndo(false);
    emit canRedo(false);

    createGrid();
}

GridWidget::GridWidget(int rowCount, int columnCount, QWidget *parent):
    QWidget(parent)
{

  mCellSize = 10;
  mRowCount = rowCount;
  mColumnCount = columnCount;

  Tool = "dot";
  ToolLength = 1;
  frontColor = Qt::black;
  backColor = QColor::Invalid;
  background = Qt::white;
  emit canUndo(false);
  emit canRedo(false);

  createGrid();
  //setMinimumSize(rowCount,columnCount);
  //switchOn(1,1,Qt::black);
}

void GridWidget::setGridSize(int rowCount, int columnCount)
{
    mRowCount = rowCount;
    mColumnCount = columnCount;

    createGrid();
}

void GridWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    drawGrid(this);
}

void GridWidget::mousePressEvent(QMouseEvent * event)
{
    if (!newStates.empty()) newStates.clear();
    if (oldStates.size() == 5) oldStates.pop_front();
    oldStates.push_back(mColors);

    emit canUndo(true);
    emit canRedo(false);

    int X = event->x() / mCellSize;
    int Y = event->y() / mCellSize;

    backColor = mColors[mColumnCount * Y + X];
    identifyTool(Tool, X, Y);

    emit cellClicked(QPoint(X,Y));
    QWidget::mousePressEvent(event);
}

void GridWidget::undo(){
    if (!oldStates.empty()){
        newStates.push_back(mColors);
        canRedo(true);
        //foreach (int index , mColors.keys()) mColors[index] = oldStates.back()[index];
        mColors.clear();
        mColors = oldStates.back();
        oldStates.pop_back();
        if (oldStates.empty()) canUndo(false);

        repaint();
    }
}

void GridWidget::redo(){
    if (!newStates.empty()){
        oldStates.push_back(mColors);
        canUndo(true);
        //foreach (int index , mColors.keys()) mColors[index] = newStates.back()[index];
        mColors.clear();
        mColors = newStates.back();
        newStates.pop_back();
        if (newStates.empty()) canRedo(false);
        repaint();
    }
}

void GridWidget::switchOn(int x, int y, const QColor &color)
{
    int index =  mColumnCount * y  + x;
    mColors[index] = color;
    //qDebug() << "(" << x << "," << y << ") " << color;
}

void GridWidget::switchOff(int x, int y)
{
    int index =  mColumnCount * y + x;
    mColors.remove(index);
}

void GridWidget::selectOn(int x, int y)
{
    int index =  mColumnCount * y + x;
    mCellSelected.append(index);
}

void GridWidget::selectOff(int x, int y)
{
    int index =  mColumnCount * y  + x;
    mCellSelected.removeOne(index);
}

void GridWidget::clear()
{
    mColors.clear();
}

void GridWidget::clearSelection()
{
    mCellSelected.clear();
}

void GridWidget::setCellSize(int size)
{
    mCellSize = size;
}

QPixmap * GridWidget::snap()
{
    QPixmap * pix = new QPixmap(size());
    drawGrid(pix);
    return pix;
}

void GridWidget::createGrid()
{
    resize(mColumnCount*mCellSize, mRowCount*mCellSize);
    setFixedSize(size());
    mGridPix = QPixmap(size());
    mGridPix.fill(background);
    QPainter paint(&mGridPix);
    paint.setPen(QPen(Qt::lightGray));

    for ( int x=0;x<width(); x+=mCellSize )
        paint.drawLine(x,rect().top(), x, rect().bottom());

    for ( int y=0;y<height(); y+=mCellSize )
        paint.drawLine(rect().left(),y,rect().right(),y);

    update();
}

void GridWidget::drawGrid(QPaintDevice *device)
{
    QPainter paint;
    paint.begin(device);

    mGridPix.fill(background);
        paint.drawPixmap(0,0,mGridPix);
    paint.setPen(QPen(Qt::lightGray));

    for ( int x=0;x<width(); x+=mCellSize )
        paint.drawLine(x,rect().top(), x, rect().bottom());

    for ( int y=0;y<height(); y+=mCellSize )
        paint.drawLine(rect().left(),y,rect().right(),y);

    //Draw Square
    foreach (int index , mColors.keys())
    {
        int y = qFloor(index/mColumnCount);
        int x = index % mColumnCount;

        if (mColors[index] != QColor::Invalid){
            paint.setBrush(mColors[index]);
            paint.drawRect(x*mCellSize, y*mCellSize, mCellSize, mCellSize);
        }
    }

    //Draw SquareSelector
    foreach (int index, mCellSelected)
    {
        int y = qFloor(index/mColumnCount);
        int x = index % mColumnCount;

        paint.setBrush(Qt::transparent);

        paint.setBrush(QBrush(Qt::Dense4Pattern));
        QRect selector = QRect(x*mCellSize,y*mCellSize, mCellSize, mCellSize);
        selector.adjust(-2,-2,2,2);
        paint.drawRect(selector);
    }

    paint.end();
}

void GridWidget::identifyTool(QString T, int x, int y){
    if (T == "dot")                 Dot(x, y, frontColor);
    else if (T == "row")            RowMajorFill(x, y, backColor, frontColor, mColumnCount);
    else if (T == "column")         ColMajorFill(x, y, backColor, frontColor, mRowCount, mColumnCount);
    else if (T == "diagonal left")  DiagonalLeft(x, y, backColor, frontColor, mRowCount, mColumnCount);
    else if (T == "diagonal right") DiagonalRight(x, y, backColor, frontColor, mRowCount, mColumnCount);
    else if (T == "square")         square(x, y, frontColor, ToolLength, mRowCount, mColumnCount);
    else if (T == "triangle")       triangle(x, y, frontColor, ToolLength, mRowCount, mColumnCount);
    else if (T == "circle")         circle(x, y, frontColor, ToolLength, mRowCount, mColumnCount);
    else qDebug() << "Tool Selection Error!";
    repaint();
}


//================= Grid View
GridView::GridView(int row, int column, QWidget *parent):
    QScrollArea(parent)
{
    mGridWidget = new GridWidget(row, column);
    setWidget(mGridWidget);
    connect(mGridWidget,SIGNAL(cellClicked(QPoint)),this,SIGNAL(cellClicked(QPoint)));
}

GridView::~GridView()
{
    delete mGridWidget;
}
