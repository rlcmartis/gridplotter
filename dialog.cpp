#include "dialog.h"
#include "ui_dialog.h"
#include "grid.cpp"

#include <QtGui>
#include <QtCore>
#include <QWidget>
#include <QGridLayout>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->undoButton->setDisabled(true);
    ui->redoButton->setDisabled(true);

    connect(ui->miWidget, SIGNAL(canUndo(bool)), this, SLOT(setUndo(bool)));
    connect(ui->miWidget, SIGNAL(canRedo(bool)), this, SLOT(setRedo(bool)));
}

void Dialog::setUndo(bool can){
    if (can == true) ui->undoButton->setDisabled(false);
    else ui->undoButton->setDisabled(true);
}

void Dialog::setRedo(bool can){
    if (can == true) ui->redoButton->setDisabled(false);
    else ui->redoButton->setDisabled(true);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_frontcolorComboBox_activated(const QString &arg1)
{
    ui->miWidget->setFront(arg1);
}

void Dialog::on_backgroundComboBox_activated(const QString &arg1)
{
    ui->miWidget->setBack(arg1);
    ui->miWidget->repaint();
}

void Dialog::on_lapizButton_clicked()
{
    ui->miWidget->setTool("dot");
}

void Dialog::on_cleargridButton_clicked()
{
    ui->miWidget->clear();
    ui->miWidget->repaint();
}

void Dialog::on_rowmajorfillButton_clicked()
{
    ui->miWidget->setTool("row");
}

void Dialog::on_colmajorfillButton_clicked()
{
    ui->miWidget->setTool("column");
}

void Dialog::on_diagonalleftButton_clicked()
{
    ui->miWidget->setTool("diagonal left");
}

void Dialog::on_diagonalrightButton_clicked()
{
    ui->miWidget->setTool("diagonal right");
}

void Dialog::on_drawsquareButton_clicked()
{
    ui->miWidget->setTool("square");
}

void Dialog::on_drawtriangleButton_clicked()
{
    ui->miWidget->setTool("triangle");
}

void Dialog::on_drawcircleButton_clicked()
{
    ui->miWidget->setTool("circle");
}

void Dialog::on_speedHorizontalSlider_valueChanged(int value)
{
    ui->miWidget->setToolLength(value);
}

void Dialog::on_undoButton_clicked()
{
    ui->miWidget->undo();
}

void Dialog::on_redoButton_clicked()
{
    ui->miWidget->redo();
}
