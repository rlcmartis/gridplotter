#include "grid.h"
#include <QDebug>
#include <QtGlobal>
/*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
   Though it is not visible in this file (tools.cpp), there exists an array
 called mColors that contains the colors of all the cells of the grid. The
 way you can obtain the index of the cell (x, y) is by applying this formula:
 number-of-columns * y + x
   For example, if we want to know the color of the cell (1,2) we use
 mColors[columns * 2 + 1] and it will give us the color.
  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  */

// Receives the coordinates of the grid where the user clicked
// and paints that cell with the color of the tool.
void GridWidget::Dot(int x, int y, QColor toolColor){
    switchOn(x, y, toolColor);
}

// Receives the coordinates of the grid where the user clicked
// and paints (from that point) the longest row of the color
// of the cell that was clicked with the color of the tool.
void GridWidget::RowMajorFill(int x, int y, QColor colorClicked, QColor toolColor, int cols){

    //For every number greater or equal than 'x', but less than 'cols', in the same 'y' clicked,
    //paint cells with 'toolColor'; stop the cycle when a cell is not the same as the 'colorClicked'
    for (int i = x; i<cols && (mColors[cols * y + i] == colorClicked) ; i++)
            switchOn(i, y, toolColor);

    //For every number less than 'x',  but greater or equal than 0, in the same 'y' clicked,
    //paint cells with 'toolColor'; stop the cycle when a cell is not the same as the 'colorClicked'
    for (int i = (x-1); i>=0 && (mColors[cols * y + i] == colorClicked) ; i--)
            switchOn(i, y, toolColor);
}

// Receives the coordinates of the grid where the user clicked
// and paints (from that point) the longest column of the color
// of the cell that was clicked with the color of the tool.
void GridWidget::ColMajorFill(int x, int y, QColor colorClicked, QColor toolColor, int rows, int cols){

    //For every number greater or equal than 'y', but less than 'rows', in the same 'x' clicked,
    //paint cells with 'toolColor'; stop the cycle when a cell is not the same as the 'colorClicked'
    for (int i = y; i<rows && (mColors[cols * i + x] == colorClicked) ; i++){
            switchOn(x, i, toolColor);
    }

    //For every number less than 'y', but greater or equal than 0, in the same 'x' clicked,
    //paint cells with 'toolColor'; stop the cycle when a cell is not the same as the 'colorClicked'
    for (int i = (y-1); i>=0 && (mColors[cols * i + x] == colorClicked) ; i--){
            switchOn(x, i, toolColor);
    }
}

// Receives the coordinates of the grid where the user clicked
// and paints (from that point) the longest left-diagonal of the color
// of the cell that was clicked with the color of the tool.
void GridWidget::DiagonalLeft(int x, int y, QColor colorClicked, QColor toolColor, int rows, int cols){

    //For every number greater or equal than 0, sum it to x and y, and paint on each iteration until
    //the number plus the coordinate exceed the columns or rows
    for (int i = x, j = y; i<cols && j<rows && (mColors[cols * j + i] == colorClicked); i++, j++){
        switchOn(i, j, toolColor);
    }

    //For every number greater than 0, subtract it to x and y, and paint on each iteration until
    //the coordinate minus the number is less than 0
    for (int i = x-1, j = y-1; i>=0 && j>=0 && (mColors[cols * j + i] == colorClicked); i--, j--){
        switchOn(i, j, toolColor);
    }
}

// Receives the coordinates of the grid where the user clicked
// and paints (from that point) the longest right-diagonal of the color
// of the cell that was clicked with the color of the tool.
void GridWidget::DiagonalRight(int x, int y, QColor colorClicked, QColor toolColor, int rows, int cols){

    //For every number greater or equal than 0, sum it to y and substract it to x, and paint
    //on each iteration until the sums do not exceed rows and substractions are not lesser than 0
    for (int i = x, j = y; i>=0 && j<rows && (mColors[cols * j + i] == colorClicked); i--, j++){
        switchOn(i, j, toolColor);
    }

    //For every number greater than 0, subtract it to y and add it to x, and paint on each
    //iteration until the sums do not exceed the columns and substractions are not lesser than 0
    for (int i = x+1, j = y-1; i<cols && j>=0 && (mColors[cols * j + i] == colorClicked); i++, j--){
        switchOn(i, j, toolColor);
    }
}

// Receives the coordinates of the grid where the user clicked
// and paints a square of the size and with the color of the tool.
void GridWidget::square(int x, int y, QColor toolColor, int toolSize, int rows, int cols){

    //For every number i between the values [x-toolSize, x+toolSize] and for every
    //j in the same range, paint all the combinations
    for (int i = -1*toolSize; i<=toolSize ; i++){
        for (int j = -1*toolSize; j<=toolSize; j++){
            if (x+i>=0 && x+i<cols && y+j>=0 && y+j<rows){
                switchOn(x+i, y+j, toolColor);
            }
        }
    }
}

// Receives the coordinates of the grid where the user clicked
// and paints a triangle of the size and with the color of the tool.
void GridWidget::triangle(int x, int y, QColor toolColor, int toolSize, int rows, int cols){

    // Paint a base of 1 + 2 * ToolSize. Then add lines over the previous one but with
    // two less pixels than the last one.
    for (int j = 0; j<=toolSize ; j++){
        for (int i = (-1*toolSize)+j; i<=toolSize-j ; i++){
            if (x+i>=0 && x+i<cols && y-j>=0 && y-j<rows)
                switchOn(x+i, y-j, toolColor);
        }
    }
}

// Receives the coordinates of the grid where the user clicked
// and paints a circle of the size and with the color of the tool.
void GridWidget::circle(int x, int y, QColor toolColor, int toolSize, int rows, int cols){

    //Here we have to use the formula of the circle which is r^2 = x^2 + y^2
    //Since we know which is the radius (toolsize), we solve for y. For every
    //number integer in [-radius, radius] we substitude x and get the y coordinate
    //for that x. Now, with that we just obtain an empty half-circle, we have to
    //complete it and paint inside of it. We will paint columns from each y to its negative.
    for (int i=-1*toolSize; i<=toolSize ; i++){
        int j = qFloor(qSqrt(qPow(toolSize,2)-qPow(i,2)));
        for (int j2 = -1*j; j2<=j ; j2++){
            if (x+i>=0 && x+i<cols && y+j2>=0 && y+j2<rows)
                switchOn(x+i, y+j2, toolColor);
        }
    }
}
