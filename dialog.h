#ifndef DIALOG_H
#define DIALOG_H

#include "grid.h"
#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:

    void on_frontcolorComboBox_activated(const QString &arg1);

    void on_backgroundComboBox_activated(const QString &arg1);

    void on_lapizButton_clicked();

    void on_cleargridButton_clicked();

    void on_rowmajorfillButton_clicked();

    void on_colmajorfillButton_clicked();

    void on_diagonalleftButton_clicked();

    void on_diagonalrightButton_clicked();

    void on_drawsquareButton_clicked();

    void on_drawtriangleButton_clicked();

    void on_drawcircleButton_clicked();

    void on_speedHorizontalSlider_valueChanged(int value);

    void on_undoButton_clicked();

    void on_redoButton_clicked();

    void setUndo(bool cannot);
    void setRedo(bool cannot);

private:
    Ui::Dialog *ui;
    GridWidget *grid;
};

#endif // DIALOG_H
